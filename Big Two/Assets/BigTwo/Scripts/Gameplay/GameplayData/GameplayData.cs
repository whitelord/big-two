﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwo
{
    public class GameplayData
    {
        public RoundData CurrentRound { get; set; }
        public int StartPlayerIndex { get; set; }
        public List<CardDataPack> DividedPacks { get; set; }

        public GameplayData()
        {
            StartPlayerIndex = -1;
        }

        public void Clear()
        {
            CurrentRound = null;
            StartPlayerIndex = -1;
            DividedPacks = null;
        }
    }
}


