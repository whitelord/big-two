﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace BigTwo
{
    [Serializable]
    public class RoundData
    {
        private List<Player> _participatePlayers;
        private Player _latestPlayedPlayer;
        private Player _currentPlayer;
        private CardPlayData _currentCardPlay;
        private int _startIndex;
        private int _roundNumber;

        private int _currentIndex;

        public CardPlayData CurrentCardPlay => _currentCardPlay;
        public int RoundNumber => _roundNumber;

        public RoundData(List<Player> playerList, int startIndex,int roundNumber)
        {
            _participatePlayers = new List<Player>(playerList);
            _startIndex = startIndex;
            _roundNumber = roundNumber;
        }

        public Player GetNextPlayer()
        {
            Player current = _participatePlayers[_startIndex];
            _currentIndex = _startIndex;

            IncrementStartIndex();

            return current;
        }

        public void UpdateCurrentPlay(CardPlayData data,Player player)
        {
            _currentCardPlay = data;
            _latestPlayedPlayer = player;
        }

        public Player GetCurrentPlayer() => _participatePlayers[_currentIndex];

        public Player GetLatestPlayedPlayer() => _latestPlayedPlayer;

        public void RemoveCurrentPlayer()
        {
            int nextIndex = _currentIndex + 1;
            nextIndex %= _participatePlayers.Count;

            Player nextPlayer = _participatePlayers[nextIndex];

            _participatePlayers.RemoveAt(_currentIndex);

            //update start index to correct next player
            _startIndex = _participatePlayers.IndexOf(nextPlayer);

        }

        public int GetCurrentPlayerCount() => _participatePlayers.Count;

        private void IncrementStartIndex()
        {
            _startIndex++;
            _startIndex %= _participatePlayers.Count;
        }


    }
}

