﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JasonWeimann.StateMachine;
using System;

namespace BigTwo
{
    public class GameManager : MonoBehaviour
    {
        private static GameManager _instance;

        public static GameManager Instance => _instance;

        [SerializeField] private PlayCardsController _playCardsController;
        [SerializeField] private List<Player> _playerList;
        [SerializeField] private RoundStartPanel _roundStartPanel;
        [SerializeField] private RoundUIPanel _roundEndPanel;
        [SerializeField] private GameOverPanel _gameOverPanel;

        private StateMachine _stateMachine;

        private GameplayData _gameplayData;

        public bool IsRestartTriggered { get; set; }

        private void Awake()
        {
            _instance = this;
        }

        private void Start()
        {
            StartGame();
        }

        private void OnEnable()
        {
            _gameOverPanel.OnQuit += QuitGame;
            _gameOverPanel.OnRestart += RestartGame;
        }

        private void OnDisable()
        {
            _gameOverPanel.OnQuit -= QuitGame;
            _gameOverPanel.OnRestart -= RestartGame;
        }

        private void StartGame()
        {
            _gameplayData = new GameplayData();

            _stateMachine = new StateMachine(this);

            var divideCardsState = new DivideCardsState(_playerList.Count,_gameplayData);
            var distributeCardsState = new DistributeCardsState(_playerList,_gameplayData);
            var determineFirstRoundTurnState = new DetermineFirstRoundTurnState(_playerList,_gameplayData);
            var createRoundState = new CreateRoundState(_playerList,_gameplayData);
            var checkRoundState = new CheckRoundState(_gameplayData);
            var roundEndedState = new RoundEndedState(_gameplayData);
            var playerTurnState = new PlayerTurnState(_gameplayData);
            var checkWinState = new CheckWinState(_gameplayData);
            var updateRoundParticipantState = new UpdateRoundParticipantState(_gameplayData);
            var haveWinnerState = new HaveWinnerState(_gameplayData);

            _stateMachine.AddTransition(divideCardsState, distributeCardsState, IsCardsDivided());
            _stateMachine.AddTransition(distributeCardsState, determineFirstRoundTurnState, IsCardsDistributed());
            _stateMachine.AddTransition(determineFirstRoundTurnState, createRoundState, IsFirstRoundTurnDetermined());

            _stateMachine.AddTransition(createRoundState, checkRoundState, IsRoundCreated());
            _stateMachine.AddTransition(checkRoundState, playerTurnState, IsRoundStillOngoing());
            _stateMachine.AddTransition(checkRoundState, roundEndedState, HasRoundWinner());
            _stateMachine.AddTransition(roundEndedState, createRoundState, IsRoundEndedCompleted());

            _stateMachine.AddTransition(playerTurnState, checkWinState, IsPlayerPlayed());
            _stateMachine.AddTransition(playerTurnState, updateRoundParticipantState, IsPlayerPassed());

            _stateMachine.AddTransition(updateRoundParticipantState, checkRoundState, PlayNextTurnAfterPass());

            _stateMachine.AddTransition(checkWinState, checkRoundState, PlayNextTurnAfterPlay());
            _stateMachine.AddTransition(checkWinState, haveWinnerState, HasWinner());

            _stateMachine.AddTransition(haveWinnerState, divideCardsState, IsRestartGame());

            //initial state
            _stateMachine.SetState(divideCardsState);

            Func<bool> IsCardsDivided() => () => _gameplayData.DividedPacks != null;
            Func<bool> IsCardsDistributed() => () => distributeCardsState.Completed;
            Func<bool> IsFirstRoundTurnDetermined() => () => _gameplayData.StartPlayerIndex != -1;
            Func<bool> IsRoundCreated() => () => _gameplayData.CurrentRound != null;
            Func<bool> HasRoundWinner() => () => checkRoundState.IsRoundFinished;
            Func<bool> IsRoundEndedCompleted() => () => roundEndedState.IsSetupCompleted;
            Func<bool> IsRoundStillOngoing() => () => !checkRoundState.IsRoundFinished;
            Func<bool> IsPlayerPlayed() => () => playerTurnState.IsTurnCompleted && !playerTurnState.IsPlayerPass;
            Func<bool> IsPlayerPassed() => () => playerTurnState.IsTurnCompleted && playerTurnState.IsPlayerPass;
            Func<bool> PlayNextTurnAfterPass() => () => updateRoundParticipantState.Completed;
            Func<bool> PlayNextTurnAfterPlay() => () => checkWinState.CompletedCheck && !checkWinState.HasWinner;
            Func<bool> HasWinner() => () => checkWinState.CompletedCheck && checkWinState.HasWinner;
            Func<bool> IsRestartGame() => () => IsRestartTriggered;
            
        }

        private void Update()
        {
            if(_stateMachine != null)
            {
                _stateMachine.Tick();
            }
        }

        public void HideAllPlayersPassIndicator()
        {
            foreach(Player p in _playerList)
            {
                p.ResetPassCondition();
            }
        }

        public void SetPlayerAsStartIndex(Player player)
        {
            int index = _playerList.IndexOf(player);
            _gameplayData.StartPlayerIndex = index;
        }

        public void UpdateCenterCards(List<CardData> cardList,Vector2 spawnPosition)
        {
            if(cardList != null)
            {
                _playCardsController.SpawnCards(cardList, spawnPosition);
            }
            else
            {
                _playCardsController.Clear();
            }
        }

        public void SetPlayerToHappy(Player player)
        {
            player.SetAvatarToHappy();
        }

        public void SetAllPlayerToSadExcept(Player player)
        {
            foreach(Player p in _playerList)
            {
                if (p == player) continue;
                p.SetAvatarToSad();
            }
        }

        public void SetAllPlayerToNormalAvatar()
        {
            foreach(Player p in _playerList)
            {
                p.RefreshAvatarToNormal();
            }
        }

        public void ShowRoundStartPanel(int roundNumber)
        {
            _roundStartPanel.SetText(roundNumber.ToString());
            _roundStartPanel.gameObject.SetActive(true);
        }

        public void HideRoundStartPanel()
        {
            _roundStartPanel.Hide();
        }

        public void ShowRoundEndPanel()
        {
            _roundEndPanel.gameObject.SetActive(true);
        }

        public void HideRoundEndPanel()
        {
            _roundEndPanel.Hide();
        }

        public void ShowWinPanel()
        {
            AudioController.Play(AudioKeys.SFX_WIN);
            _gameOverPanel.ShowPlayerWin();
        }

        public void ShowLosePanel()
        {
            AudioController.Play(AudioKeys.SFX_LOSE);
            _gameOverPanel.ShowPlayerLose();
        }

        public void HideGameOverPanel()
        {
            _gameOverPanel.gameObject.SetActive(false);
        }

        public void ResetGameplayData()
        {
            _gameplayData.Clear();
            _playCardsController.Clear();
        }

        public void ResetPlayers()
        {
            foreach(Player p in _playerList)
            {
                p.ResetData();
            }
        }

        public void RestartGame()
        {
            AudioController.Play(AudioKeys.SFX_CLICK);
            IsRestartTriggered = true;
            _gameOverPanel.gameObject.SetActive(false);
        }

        public void QuitGame()
        {
            AudioController.Play(AudioKeys.SFX_CLICK);
            Application.Quit();
        }
    }

}

