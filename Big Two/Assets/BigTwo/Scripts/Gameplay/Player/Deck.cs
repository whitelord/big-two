﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwo
{
    public class Deck : MonoBehaviour
    {
        [SerializeField] private float _cardScale;
        [SerializeField] private float _cardSizeX;
        [SerializeField] private float _cardOffset;
        [SerializeField] private List<Card> _cardList;

        private bool _isAI;
        private bool _isPlayerOwn;

        private void Start()
        {
            _cardList = new List<Card>();
        }

        public void SetCardPacks(CardDataPack pack,bool isAI,bool isPlayerOwn = false)
        {
            _isAI = isAI;
            _isPlayerOwn = isPlayerOwn;
            Refresh(pack);
        }

        private void DrawDeck(CardDataPack pack)
        {
            int cardCount = pack.CardDataList.Count;

            float startX = (-(cardCount * _cardOffset) / 2f) + _cardOffset;

            int z = cardCount;

            for(int i = 0; i < cardCount; i++)
            {
                CardData data = pack.CardDataList[i];

                GameObject g = CardObjectPool.Instance.GetCardObject();
                g.transform.SetParent(transform);
                g.transform.localScale = new Vector3(_cardScale, _cardScale, 1);
                g.transform.localPosition = new Vector3(startX + (i * _cardOffset), 0, z);
                g.SetActive(true);

                Card card = g.GetComponent<Card>();
                card.Init(data, _isAI,_isPlayerOwn);

                z--;

                _cardList.Add(card);

            }
        }

        public void Refresh(CardDataPack pack)
        {
            DespawnAllCards();
            DrawDeck(pack);
        }

        public void Clear()
        {
            DespawnAllCards();
        }

        private void DespawnAllCards()
        {
            foreach(Card c in _cardList)
            {
                CardObjectPool.Instance.Despawn(c.gameObject);
            }
            _cardList.Clear();
        }

        public int GetCardCount()
        {
            if (_cardList == null) return 0;

            return _cardList.Count;
        }

        public void CancelAllHighlight()
        {
            foreach (Card c in _cardList)
            {
                c.CancelHighlight();
            }
        }
    }
}


