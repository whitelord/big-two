﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwo
{
    public class RealPlayer : Player
    {
        private RealPlayerActionUIController _actionUIController;

        private CardPlayData _myCardPlay;

        private void OnValidate()
        {
            _playerType = PlayerType.RealPlayer;
        }

        private void Awake()
        {
            _actionUIController = GetComponent<RealPlayerActionUIController>();
            _myCardPlay = new CardPlayData();
        }

        private void OnEnable()
        {
            Card.OnClicked += OnCardSelected;
        }

        private void OnDisable()
        {
            Card.OnClicked -= OnCardSelected;
        }

        protected override AvatarData GetAvatarData()
        {
            return PlayerData.Instance.SelectedAvatar;
        }

        protected override void InitDeck()
        {
            _cardDataPack.Sort();
            _deck.SetCardPacks(_cardDataPack, false,true);
        }

        protected override void StartPlay()
        {
            
            SetStartTurn();
        }

        private void SetStartTurn()
        {
            //action UI
            UpdatePassButton(IsFirstMove());
            _actionUIController.ShowAction();

            //because player can pre-select cards when other players play their turn
            //we need to validate their selection at start turn
            ValidateSelectedCards();
        }

        private void UpdatePassButton(bool firstMove)
        {
            if (firstMove)
            {
                _actionUIController.HidePassButton();
            }
            else
            {
                _actionUIController.ShowPassButton();
            }
        }

        private bool IsFirstMove()
        {
            return _currentRoundPlay == null;
        }

        private void OnCardSelected(Card card)
        {
            if (_isPassed) return;

            bool isSelected = card.ToggleHighlight();

            if (isSelected)
            {
                _myCardPlay.AddCard(card.CardData);
            }
            else
            {
                _myCardPlay.RemoveCard(card.CardData);
            }

            ValidateSelectedCards();
        }

        private void ValidateSelectedCards()
        {
            //no cards selected
            if (_myCardPlay.CardCount == 0)
            {
                _actionUIController.DisablePlayButton();
                _actionUIController.HideErrorText();
                return;
            }

            if (_myCardPlay.IsValid())
            {
                //player start turn in first round
                if (_isFirstRound)
                {
                    ValidateFirstRoundPlay();
                }
                else
                {
                    ValidateOtherRoundPlay();
                }
                
            }
            else
            {
                _actionUIController.SetErrorPlayText("Invalid play.");
                _actionUIController.DisablePlayButton();
            }
        }

        private void ValidateFirstRoundPlay()
        {
            
            if (IsFirstMove())
            {
                //if the player move first at first round, player must use 3 diamond card
                if (_myCardPlay.IsContainLowestCard())
                {
                    _actionUIController.EnablePlayButton();
                    _actionUIController.HideErrorText();
                }
                else
                {
                    _actionUIController.SetErrorPlayText("Please use 3 Diamond first.");
                }
            }
            else
            {
                ValidateMyPlayToCurrent();
            }
        }

        private void ValidateOtherRoundPlay()
        {
            if (IsFirstMove())
            {
                _actionUIController.EnablePlayButton();
                _actionUIController.HideErrorText();
            }
            else
            {
                ValidateMyPlayToCurrent();
            }
        }

        /// <summary>
        /// Compare current selected cards to play with current round played cards
        /// </summary>
        private void ValidateMyPlayToCurrent()
        {
            if (CardPlayValidator.IsTheSamePlayType(_myCardPlay,_currentRoundPlay))
            {
                ValidateMyPlayHigherThanCurrentPlay();
            }
            else
            {
                _actionUIController.DisablePlayButton();
                _actionUIController.SetErrorPlayText("Invalid play.");
            }
        }

        private void ValidateMyPlayHigherThanCurrentPlay()
        {
            if (CardPlayValidator.IsHigherPlay(_myCardPlay,_currentRoundPlay))
            {
                _actionUIController.EnablePlayButton();
                _actionUIController.HideErrorText();
            }
            else
            {
                _actionUIController.DisablePlayButton();
                _actionUIController.SetErrorPlayText("Your cards is lower than current play.");
            }
        }

        protected override void EndTurn()
        {
            _actionUIController.HideAction();
            _deck.CancelAllHighlight();

            _myCardPlay = new CardPlayData();
        }

        public void PlayClicked()
        {
            SubmitPlay(_myCardPlay);
        }

        public void PassClicked()
        {
            AudioController.Play(AudioKeys.SFX_CLICK);
            PassPlay();
        }
    }
}


