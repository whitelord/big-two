﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace BigTwo
{
    public class RealPlayerActionUIController : MonoBehaviour
    {

        [SerializeField] private GameObject actionPanel;
        [SerializeField] private Button playButton;
        [SerializeField] private Button passButton;
        [SerializeField] private TextMeshProUGUI errorPlayText;

        private void OnEnable()
        {
            HideErrorText();
        }

        public void ShowAction()
        {
            actionPanel.SetActive(true);
        }

        public void HideAction()
        {
            actionPanel.SetActive(false);
        }

        public void DisablePlayButton()
        {
            playButton.interactable = false;
        }

        public void EnablePlayButton()
        {
            playButton.interactable = true;
        }

        public void ShowPassButton()
        {
            passButton.gameObject.SetActive(true);
        }

        public void HidePassButton()
        {
            passButton.gameObject.SetActive(false);
        }

        public void HideErrorText()
        {
            errorPlayText.gameObject.SetActive(false);
        }

        public void SetErrorPlayText(string text)
        {
            errorPlayText.gameObject.SetActive(true);
            errorPlayText.text = text;
        }

    }
}


