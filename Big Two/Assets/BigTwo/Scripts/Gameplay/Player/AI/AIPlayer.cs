﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwo
{
    public class AIPlayer : Player
    {
        private AIPlayerActionUIController _actionUIController;

        private AIBrain _aiBrain;

        private void Awake()
        {
            _actionUIController = GetComponent<AIPlayerActionUIController>();
        }

        private void OnValidate()
        {
            _playerType = PlayerType.AI;
        }

        protected override AvatarData GetAvatarData()
        {
            return AvatarManager.Instance.GetRandomAvatar();
        }

        protected override void InitDeck()
        {
            _cardDataPack.Sort();
            _deck.SetCardPacks(_cardDataPack,true);

            _aiBrain = new AIBrain(_cardDataPack);
        }

        protected override void StartPlay()
        {
            StartCoroutine(PlayCard());
        }

        private IEnumerator PlayCard()
        {
            _actionUIController.ShowThinkingIndicator();

            CardPlayData playData = FindPlay();

            yield return new WaitForSeconds(1f);

            if(playData == null)
            {
                PassPlay();
            }
            else
            {
                SubmitPlay(playData);
            }

            _actionUIController.HideThinkingIndicator();
            
        }

        private CardPlayData FindPlay()
        {
            if (_isFirstRound)
            {
                if(_currentRoundPlay == null)
                {
                    return _aiBrain.GetFirstRoundLowestCardToPlay();
                }
                else
                {
                    return _aiBrain.GetHigherPlay(_currentRoundPlay);
                }
            }
            else
            {
                if(_currentRoundPlay == null)
                {
                    return _aiBrain.GetStarterCardToPlay();
                }
                else
                {
                    return _aiBrain.GetHigherPlay(_currentRoundPlay);
                }
            }
            
        }

        protected override void EndTurn()
        {
            
        }
    }
}

