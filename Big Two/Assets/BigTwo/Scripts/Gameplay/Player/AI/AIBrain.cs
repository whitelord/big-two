﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace BigTwo
{
    public class AIBrain
    {
        private Dictionary<PlayType, List<CardPlayData>> _grouppedPlays;

        private List<CardData> _cardList;

        private Dictionary<int, List<CardDataPack>> _grouppedCards;

        public AIBrain(CardDataPack pack)
        {
            _grouppedPlays = new Dictionary<PlayType, List<CardPlayData>>();
            _grouppedCards = new Dictionary<int, List<CardDataPack>>();

            CreateGrouppedPlays(pack);
        }


        /// <summary>
        /// for getting play contains 3 diamond
        /// </summary>
        /// <returns></returns>
        public CardPlayData GetFirstRoundLowestCardToPlay()
        {
            CardPlayData selectedPlay = null;

            foreach (PlayType type in _grouppedPlays.Keys)
            {
                List<CardPlayData> playList = _grouppedPlays[type];

                foreach (CardPlayData play in playList)
                {
                    if (play.IsContainLowestCard())
                    {
                        selectedPlay = play;
                        break;
                    }
                }

                if (selectedPlay != null)
                {
                    RemovePlayFromGroups(selectedPlay);
                    break;
                }
            }

            return selectedPlay;
        }

        /// <summary>
        /// For open round play
        /// </summary>
        /// <returns></returns>
        public CardPlayData GetStarterCardToPlay()
        {
            CardPlayData play = null;

            if (_grouppedPlays.ContainsKey(PlayType.Five))
            {
                play = _grouppedPlays[PlayType.Five][0];
                
            }
            else if (_grouppedPlays.ContainsKey(PlayType.Triple))
            {
                play = _grouppedPlays[PlayType.Triple][0];
            }
            else if(_grouppedPlays.ContainsKey(PlayType.Pair))
            {
                play = _grouppedPlays[PlayType.Pair][0];
            }
            else if(_grouppedPlays.ContainsKey(PlayType.Single))
            {
                play = _grouppedPlays[PlayType.Single][0];
            }

            if(play == null)
            {
                Debug.LogError("Something wrong");
                return null;
            }

            RemovePlayFromGroups(play);

            return play;
        }

        public CardPlayData GetHigherPlay(CardPlayData currentPlay)
        {
            if (!_grouppedPlays.ContainsKey(currentPlay.PlayType)) return null;

            List<CardPlayData> possiblePlays = _grouppedPlays[currentPlay.PlayType];

            CardPlayData possiblePlay = null;

            foreach (CardPlayData play in possiblePlays)
            {
                if (CardPlayValidator.IsHigherPlay(play, currentPlay))
                {
                    possiblePlay = play;
                    break;
                }
            }

            if (possiblePlay != null)
            {
                RemovePlayFromGroups(possiblePlay);
            }

            return possiblePlay;
        }

        private void CreateGrouppedPlays(CardDataPack pack)
        {
            pack.Sort();
            _cardList = new List<CardData>(pack.CardDataList);

            CreateStraightGroupCards();

            //group similar value cards
            CreateGrouppedCards();

            CreateFourOfKindGroupPlay();

            CreateFullHouseGroupPlay();

            CreateTripleGroupPlay();

            CreatePairGroupPlay();

            CreateSingleGroupPlay();
        }

        private void CreateStraightGroupCards()
        {
            int cardCount = _cardList.Count;

            for (int start = 0; start < cardCount; start++)
            {
                List<CardData> possibleStraight = new List<CardData>();
                CardData data = _cardList[start];
                possibleStraight.Add(data);

                int currentValue = data.Value;
                for (int next = start + 1; next < cardCount; next++)
                {
                    CardData nextCard = _cardList[next];

                    if (nextCard.Value - currentValue == 1)
                    {
                        possibleStraight.Add(nextCard);
                        currentValue = nextCard.Value;
                    }
                }

                //straight found
                if (possibleStraight.Count == 5)
                {
                    CardPlayData playCard = new CardPlayData();
                    playCard.AddCards(possibleStraight);
                    AddPlayCardToGroups(playCard);

                    //update card count
                    cardCount = _cardList.Count;
                }
            }
        }

        private void CreateGrouppedCards()
        {
            //quad cards
            GroupSameValueCards(4);

            //triple cards
            GroupSameValueCards(3);

            //pair
            GroupSameValueCards(2);
        }

        private void CreateFourOfKindGroupPlay()
        {
            if (_grouppedCards.ContainsKey(4))
            {
                List<CardDataPack> packList = _grouppedCards[4];
                foreach (CardDataPack pack in packList)
                {
                    CardData single = GetLowestCardAndRemoveFromCardList();

                    if (single == null) break;

                    CardPlayData playData = new CardPlayData();
                    playData.AddCards(pack.CardDataList);
                    playData.AddCard(single);

                    AddPlayCardToGroups(playData);
                }
            }
        }

        private void CreateFullHouseGroupPlay()
        {
            if (_grouppedCards.ContainsKey(3))
            {
                List<CardDataPack> packList = _grouppedCards[3];
                List<CardDataPack> toBeRemovedList = new List<CardDataPack>();
                foreach (CardDataPack pack in packList)
                {
                    CardDataPack pair = GetPairFromGroupCardsAndRemoveFromList();
                    if (pair == null) break;

                    CardPlayData playData = new CardPlayData();
                    playData.AddCards(pack.CardDataList);
                    playData.AddCards(pair.CardDataList);

                    toBeRemovedList.Add(pack);
                    AddPlayCardToGroups(playData);
                }

                //remove used pack
                foreach (CardDataPack pack in toBeRemovedList)
                {
                    packList.Remove(pack);
                }
            }
        }

        private void CreateTripleGroupPlay()
        {
            if (_grouppedCards.ContainsKey(3))
            {
                List<CardDataPack> packList = _grouppedCards[3];
                foreach (CardDataPack pack in packList)
                {
                    CardPlayData playData = new CardPlayData();
                    playData.AddCards(pack.CardDataList);

                    AddPlayCardToGroups(playData);
                }
            }
        }

        private void CreatePairGroupPlay()
        {
            if (_grouppedCards.ContainsKey(2))
            {
                List<CardDataPack> packList = _grouppedCards[2];
                foreach (CardDataPack pack in packList)
                {
                    CardPlayData playData = new CardPlayData();
                    playData.AddCards(pack.CardDataList);

                    AddPlayCardToGroups(playData);
                }
            }
        }

        private void CreateSingleGroupPlay()
        {
            foreach (CardData data in _cardList.ToArray())
            {
                CardPlayData playData = new CardPlayData();
                playData.AddCard(data);
                AddPlayCardToGroups(playData);
            }
        }

        private void GroupSameValueCards(int count)
        {
            int cardCount = _cardList.Count;

            for (int start = 0; start < cardCount; start++)
            {
                List<CardData> possibleList = new List<CardData>();

                CardData data = _cardList[start];

                possibleList.Add(data);

                int currentValue = data.Value;
                for (int next = start + 1; next < cardCount; next++)
                {
                    CardData nextCard = _cardList[next];
                    if (nextCard.Value == currentValue)
                    {
                        possibleList.Add(nextCard);
                    }
                }

                if (possibleList.Count == count)
                {
                    CardDataPack pack = new CardDataPack(possibleList);
                    AddCardsToGroups(count, pack);

                    //update card count
                    cardCount = _cardList.Count;
                }
            }

        }

        private void AddPlayCardToGroups(CardPlayData playCard)
        {
            if (_grouppedPlays.ContainsKey(playCard.PlayType))
            {
                _grouppedPlays[playCard.PlayType].Add(playCard);
            }
            else
            {
                List<CardPlayData> list = new List<CardPlayData>();
                list.Add(playCard);
                _grouppedPlays.Add(playCard.PlayType, list);
            }

            RemoveCardsFromCardList(playCard.Cards);
        }

        private void RemovePlayFromGroups(CardPlayData playData)
        {
            if (!_grouppedPlays.ContainsKey(playData.PlayType)) return;
            List<CardPlayData> playList = _grouppedPlays[playData.PlayType];

            playList.Remove(playData);

            if (playList.Count <= 0) _grouppedPlays.Remove(playData.PlayType);
        }

        private void AddCardsToGroups(int type,CardDataPack pack)
        {
            if (_grouppedCards.ContainsKey(type))
            {
                _grouppedCards[type].Add(pack);
            }
            else
            {
                List<CardDataPack> list = new List<CardDataPack>();
                list.Add(pack);
                _grouppedCards.Add(type, list);
            }

            RemoveCardsFromCardList(pack.CardDataList);
        }

        private void RemoveCardsFromCardList(List<CardData> list)
        {
            foreach(CardData data in list)
            {
                _cardList.Remove(data);
            }
        }

        private CardData GetLowestCardAndRemoveFromCardList()
        {
            if (_cardList.Count <= 0) return null;

            CardData data = _cardList[0];
            _cardList.Remove(data);
            return data;
        }

        private CardDataPack GetPairFromGroupCardsAndRemoveFromList()
        {
            if (_grouppedCards.ContainsKey(2))
            {
                List<CardDataPack> packList = _grouppedCards[2];

                if (packList.Count <= 0) return null;

                CardDataPack pack = packList[0];
                packList.Remove(pack);
                return pack;
            }
            return null;
        }
    }
}


