﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BigTwo
{
    public class AIPlayerActionUIController : MonoBehaviour
    {
        [SerializeField] private GameObject thinkingIndicator;

        public void ShowThinkingIndicator()
        {
            thinkingIndicator.SetActive(true);
        }

        public void HideThinkingIndicator()
        {
            thinkingIndicator.SetActive(false);
        }

    }
}


