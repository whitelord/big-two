﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

namespace BigTwo
{
    public class PlayerAvatar : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _renderer;
        [SerializeField] private TextMeshProUGUI _cardCountText;

        private AvatarData _avatarData;

        private Tween happyScaleTween;

        private void Awake()
        {
            CreateTween();
        }

        private void CreateTween()
        {
            happyScaleTween = _renderer.transform.DOScale(new Vector3(0.3f, 0.3f, 1), 0.1f);
            happyScaleTween.SetEase(Ease.Linear);
            happyScaleTween.SetLoops(-1, LoopType.Yoyo);
            happyScaleTween.SetAutoKill(false);
        }

        public void Init(AvatarData data)
        {
            _avatarData = data;
            RefreshAvatar();
        }

        public void UpdateCardCountText(int count)
        {
            _cardCountText.text = count.ToString();
        }

        public void RefreshAvatar()
        {
            _renderer.transform.localScale = new Vector2(0.2f, 0.2f);
            _renderer.sprite = _avatarData.MainAvatar;
            happyScaleTween.Pause();
        }

        public void SetSadAvatar()
        {
            _renderer.sprite = _avatarData.SadAvatar;
        }

        public void SetHappyAvatar()
        {
            _renderer.sprite = _avatarData.HappyAvatar;
            happyScaleTween.Restart();
            
        }

    }

}

