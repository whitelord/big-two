﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwo
{
    public abstract class Player : MonoBehaviour
    {

        [SerializeField] protected PlayerType _playerType;
        [SerializeField] protected PlayerAvatar _playerAvatar;
        [SerializeField] protected Deck _deck;

        [SerializeField] private Transform _initialSpawnPlayCardPosition;
        [SerializeField] private GameObject _passIndicatorObject;

        public Vector2 PlayCardsSpawnPosition => _initialSpawnPlayCardPosition.position;

        protected bool _isPlaying;
        protected bool _isFirstRound;
        protected CardPlayData _currentRoundPlay;
        protected bool _isPassed;

        [SerializeField]
        protected CardDataPack _cardDataPack;

        private CardPlayData _submittedPlay;
        
        public int RemainingCardCount => _cardDataPack.CardDataList.Count;
        public CardPlayData SubmittedPlay => _submittedPlay;
        public bool IsPlaying => _isPlaying;

        protected abstract void InitDeck();

        /// <summary>
        /// Called when player start their turn at round
        /// </summary>
        protected abstract void StartPlay();

        /// <summary>
        /// Called when player end their turn
        /// </summary>
        protected abstract void EndTurn();

        protected abstract AvatarData GetAvatarData();

        protected virtual void Start()
        {
            UpdatePlayerAvatar();
        }

        public void SetCardDataPack(CardDataPack pack)
        {
            _cardDataPack = pack;
            UpdateCardCountText();
            InitDeck();

        }

        protected void UpdatePlayerAvatar()
        {
            _playerAvatar.Init(GetAvatarData());
        }

        public void StartTurn(CardPlayData currentPlay,bool isFirstRound)
        {
            _isPlaying = true;
            _isFirstRound = isFirstRound;
            _currentRoundPlay = currentPlay;

            StartPlay();
        }

        protected void SubmitPlay(CardPlayData playData)
        {
            _submittedPlay = playData;

            //update current hand
            UpdateHand();

            //end player turn
            EndTurn();
            _isPlaying = false;

        }

        protected void PassPlay()
        {
            _submittedPlay = null;
            _passIndicatorObject.SetActive(true);
            _isPassed = true;

            //end turn
            EndTurn();
            _isPlaying = false;

        }

        private void UpdateHand()
        {
            RemoveCardsFromHand();
            _deck.Refresh(_cardDataPack);
            UpdateCardCountText();
        }

        private void UpdateCardCountText()
        {
            _playerAvatar.UpdateCardCountText(_cardDataPack.CardDataList.Count);
        }

        private void RemoveCardsFromHand()
        {
            _cardDataPack.RemoveCardData(_submittedPlay.Cards);
        }

        public void SetAvatarToSad()
        {
            _playerAvatar.SetSadAvatar();
        }

        public void SetAvatarToHappy()
        {
            _playerAvatar.SetHappyAvatar();
        }

        public void RefreshAvatarToNormal()
        {
            _playerAvatar.RefreshAvatar();
        }

        public void ResetPassCondition()
        {
            _passIndicatorObject.SetActive(false);
            _isPassed = false;
        }

        public bool HasLowestCard()
        {
            CardData lowestCard = CardManager.Instance.GetLowestCardValue();

            foreach(CardData data in _cardDataPack.CardDataList)
            {
                if(data.Value == lowestCard.Value && data.CardType == lowestCard.CardType)
                {
                    return true;
                }
            }

            return false;
        }

        public virtual void ResetData()
        {
            _isPlaying = false;
            _deck.Clear();
            _isFirstRound = false;
            _currentRoundPlay = new CardPlayData();
            _cardDataPack = null;
            _submittedPlay = null;
            _playerAvatar.RefreshAvatar();
            ResetPassCondition();
        }

    }

}

