﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

namespace BigTwo
{
    public class RoundStartPanel : RoundUIPanel
    {
        [SerializeField] private TextMeshProUGUI _roundText;

        public void SetText(string text)
        {
            _roundText.text = text;
        }
        
    }

}

