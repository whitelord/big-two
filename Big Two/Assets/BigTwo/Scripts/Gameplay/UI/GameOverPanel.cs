﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwo
{
    public class GameOverPanel : MonoBehaviour
    {

        public delegate void GameOverButtonHandler();
        public event GameOverButtonHandler OnQuit;
        public event GameOverButtonHandler OnRestart;

        [SerializeField] private GameObject winWindow;
        [SerializeField] private GameObject loseWindow;

        public void ShowPlayerWin()
        {
            Reset();
            winWindow.SetActive(true);
            gameObject.SetActive(true);
        }

        public void ShowPlayerLose()
        {
            Reset();
            loseWindow.SetActive(true);
            gameObject.SetActive(true);
        }

        public void QuitButtonClicked()
        {
            OnQuit?.Invoke();
        }

        public void RestartButtonClicked()
        {
            OnRestart?.Invoke();
        }

        private void Reset()
        {
            winWindow.SetActive(false);
            loseWindow.SetActive(false);
        }
    }

}

