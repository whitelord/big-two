﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwo
{
    public class RoundUIPanel : MonoBehaviour
    {
        protected Animator _animator;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        public void Hide()
        {
            _animator.SetTrigger("Hide");
        }

        private void DisableObject()
        {
            gameObject.SetActive(false);
        }
    }

}
