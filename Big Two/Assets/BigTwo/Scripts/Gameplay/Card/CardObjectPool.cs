﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwo
{
    public class CardObjectPool : MonoBehaviour
    {
        private static CardObjectPool _instance;

        public static CardObjectPool Instance => _instance;

        [SerializeField] private GameObject _cardPrefab;

        List<GameObject> _cardObjectList;

        private void Awake()
        {
            _instance = this;
            _cardObjectList = new List<GameObject>();
        }

        public GameObject GetCardObject()
        {
            foreach(GameObject g in _cardObjectList)
            {
                if (!g.activeSelf) return g;
            }

            GameObject newCard = CreateNewCard();

            return newCard;
        }

        public void Despawn(GameObject g)
        {
            g.SetActive(false);
            g.transform.SetParent(transform);
        }

        private GameObject CreateNewCard()
        {
            GameObject g = Instantiate(_cardPrefab);
            g.transform.SetParent(transform);
            g.SetActive(false);
            _cardObjectList.Add(g);
            return g;
        }

        private void OnDestroy()
        {
            _cardObjectList.Clear();
        }
    }
}


