﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

namespace BigTwo
{
    [Serializable]
    public class CardDataPack
    {
        [SerializeField]
        private List<CardData> _cardDataList;

        public List<CardData> CardDataList => _cardDataList;

        public CardDataPack()
        {
            _cardDataList = new List<CardData>();
        }

        public CardDataPack(List<CardData> cardList)
        {
            _cardDataList = cardList;
        }

        public void AddCardData(CardData data)
        {
            _cardDataList.Add(data);
        }

        public void RemoveCardData(List<CardData> cardList)
        {
            foreach(CardData data in cardList)
            {
                _cardDataList.Remove(data);
            }
        }

        public void Sort()
        {
            _cardDataList = Utils.SortCardDataList(CardDataList);
        }
    }
}

