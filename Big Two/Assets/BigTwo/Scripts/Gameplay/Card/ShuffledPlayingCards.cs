﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwo
{
    public class ShuffledPlayingCards
    {

        private List<int> _availableCardIndexes;
        private List<CardDataPack> _cardPackList;
        private int _maximumCardCount;

        public List<CardDataPack> CardPacks => _cardPackList;

        private int _divider;

        public ShuffledPlayingCards(int divider)
        {
            _divider = divider;
            _maximumCardCount = CardManager.Instance.GetMaximumCardCount();
            PopulateCardDataPool();
            DivideCards();
        }

        private void PopulateCardDataPool()
        {
            _availableCardIndexes = new List<int>();

            for(int i = 0; i < _maximumCardCount; i++)
            {
                _availableCardIndexes.Add(i);
            }
        }

        private void DivideCards()
        {
            int cardCountPerPlayer = _maximumCardCount / _divider;

            _cardPackList = new List<CardDataPack>();

            for(int i = 0; i < _divider; i++)
            {
                CardDataPack pack = new CardDataPack();

                for(int j = 0; j < cardCountPerPlayer; j++)
                {
                    pack.AddCardData(GetRandomCardData());
                }

                _cardPackList.Add(pack);
            }

        }

        private CardData GetRandomCardData()
        {
            //get random cardDataIndex
            int random = Random.Range(0, _availableCardIndexes.Count);
            int cardDataIndex = _availableCardIndexes[random];

            //get card data based on random card index
            CardData data = CardManager.Instance.GetCardDataAtIndex(cardDataIndex);

            //remove the index from available card indexes
            _availableCardIndexes.Remove(cardDataIndex);

            return data;
        }
    }
}


