﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;

namespace BigTwo
{
    public class Card : MonoBehaviour
    {
        public delegate void ClickHandler(Card card);
        public static event ClickHandler OnClicked;

        [SerializeField] private CardData _cardData;
        [SerializeField] private SpriteRenderer _renderer;

        private bool _isClosed;
        private bool _isPlayerOwn;
        private bool _isHighlighted;

        private Transform _spriteTransform;

        public CardData CardData => _cardData;
        public bool IsClosed => _isClosed;


        private void Awake()
        {
            _spriteTransform = _renderer.transform;
        }

        private void OnDisable()
        {
            ResetCard();
        }

        public void Init(CardData data,bool isClosed,bool isPlayerOwn = false)
        {
            _cardData = data;
            _isClosed = isClosed;
            _isPlayerOwn = isPlayerOwn;
            RefreshRenderer();
        }

        public void Init(CardData data)
        {
            Init(data, false);
        }

        private void RefreshRenderer()
        {
            if (_isClosed)
            {
                ShowClosedCard();
            }
            else
            {
                ShowOpenCard();
            }
            
        }

        private void ShowClosedCard() => _renderer.sprite = CardManager.Instance.CloseCardSprite;

        private void ShowOpenCard() => _renderer.sprite = _cardData.SpriteImage;

        public void OnMouseUpAsButton()
        {
            if (_isPlayerOwn)
            {
                OnClicked?.Invoke(this);
            }
            
        }

        public bool ToggleHighlight()
        {
            if (!_isHighlighted)
            {
                SetHightlight();
            }
            else
            {
                CancelHighlight();
            }

            return _isHighlighted;

        }

        private void SetHightlight()
        {
            _isHighlighted = true;
            Tween t = _spriteTransform.DOLocalMoveY(0.35f, 0.1f);
            t.SetEase(Ease.Linear);
            t.SetLoops(1, LoopType.Restart);
            t.Play();
        }

        public void CancelHighlight()
        {
            _isHighlighted = false;
            Tween t = _spriteTransform.DOLocalMoveY(0, 0.1f);
            t.SetEase(Ease.Linear);
            t.SetLoops(1, LoopType.Restart);
            t.Play();
        }

        private void ResetCard()
        {
            Vector2 currentSpritePosition = _spriteTransform.localPosition;
            currentSpritePosition.y = 0;
            _spriteTransform.localPosition = currentSpritePosition;

            _isHighlighted = false;
        }

    }
}


