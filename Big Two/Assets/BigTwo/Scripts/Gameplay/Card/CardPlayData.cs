﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace BigTwo
{
    public enum PlayType
    {
        Unknown,
        Single,
        Pair,
        Triple,
        Five
    }

    public enum FiveCardPlayType
    {
        None = 0,
        Straight = 1,
        Flush = 2,
        FullHouse = 3,
        FourOfKind = 4,
        StraightFlush = 5
    }

    public class CardPlayData
    {
        private PlayType _playType;
        private FiveCardPlayType _fiveCardPlayType;
        private List<CardData> _cards;

        private int _highestValue;
        private int _highestCardType;

        public PlayType PlayType => _playType;

        public List<CardData> Cards => _cards;
        public int CardCount => _cards.Count;

        public int HighestValue => _highestValue;
        public int HighestType => _highestCardType;
        public int FiveCardsValue => (int)_fiveCardPlayType;

        public CardPlayData()
        {
            _cards = new List<CardData>();
        }

        public void AddCards(List<CardData> list)
        {
            _cards.AddRange(list);
            Recalculate();
        }

        public void AddCard(CardData card)
        {
            _cards.Add(card);
            Recalculate();
        }

        private void Recalculate()
        {
            ResetValues();
            CheckPlayType();
            FindHighestValueAndType();
        }

        public void RemoveCard(CardData card)
        {
            _cards.Remove(card);
            ResetValues();
            CheckPlayType();
            FindHighestValueAndType();
        }

        private void ResetValues()
        {
            _highestValue = 0;
            _highestCardType = 0;
            _playType = PlayType.Unknown;
            _fiveCardPlayType = FiveCardPlayType.None;
        }

        private void CheckPlayType()
        {

            if (CardPlayValidator.IsSingle(_cards))
            {
                _playType = PlayType.Single;
            }
            else if (CardPlayValidator.IsPair(_cards))
            {
                _playType = PlayType.Pair;
            }
            else if (CardPlayValidator.IsTriple(_cards))
            {
                _playType = PlayType.Triple;
            }
            else if(CardPlayValidator.IsFive(_cards,out var fiveType))
            {
                _playType = PlayType.Five;
                _fiveCardPlayType = fiveType;
            }

        }

        private void FindHighestValueAndType()
        {
            switch (_playType)
            {
                case PlayType.Single:
                    _highestValue = _cards[0].Value;
                    _highestCardType = (int)_cards[0].CardType;
                    break;

                case PlayType.Pair:
                    _highestValue = _cards[0].Value;
                    FindHighestTypeInSameValueCards();
                    break;

                case PlayType.Triple:
                    _highestValue = _cards[0].Value;
                    FindHighestTypeInSameValueCards();
                    break;

                case PlayType.Five:
                    FindHighestValueAndTypeInFiveCardsPlay();
                    break;

            }
        }

        private void FindHighestValueAndTypeInFiveCardsPlay()
        {
            switch (_fiveCardPlayType)
            {
                case FiveCardPlayType.Flush:
                    _highestCardType = (int)_cards[0].CardType;
                    _highestValue = _cards.OrderByDescending(item => item.Value).First().Value;
                    break;

                case FiveCardPlayType.Straight:
                    CardData card = _cards.OrderByDescending(item => item.Value).First();
                    _highestValue = card.Value;
                    _highestCardType = (int)card.CardType;
                    break;

                case FiveCardPlayType.StraightFlush:
                    _highestCardType = (int)_cards[0].CardType;
                    _highestValue = _cards.OrderByDescending(item => item.Value).First().Value;
                    break;

                case FiveCardPlayType.FullHouse:
                    FindHighestValueAndTypeInSpecialFive();
                    break;

                case FiveCardPlayType.FourOfKind:
                    FindHighestValueAndTypeInSpecialFive();
                    break;
            }
        }

        private void FindHighestTypeInSameValueCards(List<CardData> cardList = null)
        {
            cardList = cardList ?? _cards;

            foreach (CardData data in cardList)
            {
                int type = (int)data.CardType;
                if (type > _highestCardType) _highestCardType = type;
            }
        }

        private void FindHighestValueAndTypeInSpecialFive()
        {
            Dictionary<int, List<CardData>> groups = new Dictionary<int, List<CardData>>();

            foreach(CardData card in _cards)
            {
                if (groups.ContainsKey(card.Value))
                {
                    groups[card.Value].Add(card);
                }
                else
                {
                    List<CardData> list = new List<CardData>();
                    list.Add(card);
                    groups.Add(card.Value, list);
                }
            }

            List<CardData> biggestList = null;

            foreach(int val in groups.Keys)
            {
                if (biggestList == null) biggestList = groups[val];

                int count = groups[val].Count;
                if(biggestList.Count < count)
                {
                    biggestList = groups[val];
                }
            }

            _highestValue = biggestList[0].Value;
            FindHighestTypeInSameValueCards(biggestList);

        }

        public bool IsContainLowestCard()
        {
            CardData lowestCard = CardManager.Instance.GetLowestCardValue();

            foreach (CardData data in _cards)
            {
                if (data.Value == lowestCard.Value && data.CardType == lowestCard.CardType)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Is cards have valid play type(single, pair, triple, five)
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            return _playType != PlayType.Unknown;
        }

        public void PrintDebug()
        {
            Debug.Log("Highest Value : " + _highestValue.ToString() + ", highest Type : " + _highestCardType.ToString());
        }
    }
}


