﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwo
{
    public class CardPlayValidator
    {

        public static bool IsSingle(List<CardData> cards)
        {
            return cards.Count == 1;
        }

        public static bool IsPair(List<CardData> cards)
        {
            if (cards.Count != 2) return false;

            if (cards[0].Value == cards[1].Value) return true;

            return false;
        }

        public static bool IsTriple(List<CardData> cards)
        {
            if (cards.Count != 3) return false;

            int val = cards[0].Value;
            for(int i = 1; i < cards.Count; i++)
            {
                if (val != cards[i].Value) return false;
            }

            return true;
        }

        public static bool IsFive(List<CardData> cards, out FiveCardPlayType fiveType)
        {
            fiveType = FiveCardPlayType.None;

            if (cards.Count != 5) return false;

            if (IsStraightFlush(cards))
            {
                fiveType = FiveCardPlayType.StraightFlush;
            }
            else if (IsStraight(cards))
            {
                fiveType = FiveCardPlayType.Straight;
            }
            else if (IsFlush(cards))
            {
                fiveType = FiveCardPlayType.Flush;
            }
            else if (IsFullHouse(cards))
            {
                fiveType = FiveCardPlayType.FullHouse;
            }
            else if (IsFourOfKind(cards))
            {
                fiveType = FiveCardPlayType.FourOfKind;
            }
            else
            {
                return false;
            }

            return true;
        }

        public static bool IsStraight(List<CardData> cards)
        {
            cards = Utils.SortCardDataList(cards);

            int previous = cards[0].Value;

            for(int i = 1; i < cards.Count; i++)
            {
                int val = cards[i].Value;
                if (val - previous != 1) return false;
                previous = val;
            }

            return true;

        }

        public static bool IsFlush(List<CardData> cards)
        {
            CardType previous = cards[0].CardType;

            for (int i = 1; i < cards.Count; i++)
            {
                CardType current = cards[i].CardType;
                if (current != previous) return false;
            }

            return true;

        }

        public static bool IsStraightFlush(List<CardData> cards)
        {
            return IsStraight(cards) && IsFlush(cards);
        }

        public static bool IsFullHouse(List<CardData> cards)
        {
            int firstVal = 0;
            int secondVal = 0;

            int firstValOccurence = 0;
            int secondValOccurence = 0;

            for(int i = 0; i < cards.Count; i++)
            {
                int val = cards[i].Value;

                if(firstVal == 0)
                {
                    firstVal = val;
                    firstValOccurence = 1;
                }
                else if(val == firstVal)
                {
                    firstValOccurence += 1;
                    continue;
                }
                else if(secondVal == 0)
                {
                    secondVal = val;
                    secondValOccurence = 1;
                }
                else if(val == secondVal)
                {
                    secondValOccurence += 1;
                    continue;
                }
                else
                {
                    return false;
                }
            }

            return Mathf.Abs(firstValOccurence - secondValOccurence) == 1;
        }

        public static bool IsFourOfKind(List<CardData> cards)
        {
            int firstVal = 0;
            int secondVal = 0;

            int firstValOccurence = 0;
            int secondValOccurence = 0;

            for (int i = 0; i < cards.Count; i++)
            {
                int val = cards[i].Value;

                if (firstVal == 0)
                {
                    firstVal = val;
                    firstValOccurence = 1;
                }
                else if (val == firstVal)
                {
                    firstValOccurence += 1;
                    continue;
                }
                else if (secondVal == 0)
                {
                    secondVal = val;
                    secondValOccurence = 1;
                }
                else if (val == secondVal)
                {
                    secondValOccurence += 1;
                    continue;
                }
                else
                {
                    return false;
                }
            }

            return Mathf.Abs(firstValOccurence - secondValOccurence) == 3;
        }

        public static bool IsTheSamePlayType(CardPlayData play1,CardPlayData play2)
        {
            return play1.PlayType == play2.PlayType;
        }

        public static bool IsHigherPlay(CardPlayData toBeCompare,CardPlayData comparator)
        {

            if(toBeCompare.PlayType == PlayType.Five)
            {
                if (comparator.FiveCardsValue > toBeCompare.FiveCardsValue) return false;
                else if (comparator.FiveCardsValue < toBeCompare.FiveCardsValue) return true;
            }

            if (comparator.HighestValue > toBeCompare.HighestValue) return false;

            if(comparator.HighestValue == toBeCompare.HighestValue)
            {
                if (comparator.HighestType > toBeCompare.HighestType) return false;
            }

            return true;
        }
    }
}


