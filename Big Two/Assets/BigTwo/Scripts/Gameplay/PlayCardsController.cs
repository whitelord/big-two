﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace BigTwo
{
    public class PlayCardsController : MonoBehaviour
    {
        [SerializeField] private float _cardScale;
        [SerializeField] private float _cardSizeX;
        [SerializeField] private float _cardOffset;

        [SerializeField] private Transform _cardParentTransfrom;

        private List<Card> _currentSpawnedCards;
        private List<CardData> _currentCardDataList;

        private void Awake()
        {
            _currentSpawnedCards = new List<Card>();
        }

        public void SpawnCards(List<CardData> cardList,Vector2 worldPosition)
        {
            _currentCardDataList = cardList;

            Clear();

            _cardParentTransfrom.position = worldPosition;
            
            DrawCards();

            AnimateToCenter();

            AudioController.Play(AudioKeys.SFX_CARD_DRAW);
        }

        private void DrawCards()
        {
            int cardCount = _currentCardDataList.Count;

            float startX = (-(cardCount * _cardOffset) / 2f) + _cardOffset;

            int z = cardCount;
            
            for (int i = 0; i < cardCount; i++)
            {
                CardData data = _currentCardDataList[i];

                GameObject g = CardObjectPool.Instance.GetCardObject();
                g.transform.SetParent(_cardParentTransfrom);
                g.transform.localScale = new Vector3(_cardScale, _cardScale, 1);
                g.transform.localPosition = new Vector3(startX + (i * _cardOffset), 0, z);
                g.SetActive(true);

                Card card = g.GetComponent<Card>();
                card.Init(data);

                z--;

                _currentSpawnedCards.Add(card);

            }
            
        }

        private void AnimateToCenter()
        {
            Tween t = _cardParentTransfrom.DOLocalMove(Vector2.zero, 0.2f);
            t.SetEase(Ease.InSine);
            t.SetLoops(1, LoopType.Restart);
            t.Play();
        }

        public void Clear()
        {
            foreach(Card c in _currentSpawnedCards)
            {
                CardObjectPool.Instance.Despawn(c.gameObject);
            }

            _currentSpawnedCards.Clear();
        }

    }
}


