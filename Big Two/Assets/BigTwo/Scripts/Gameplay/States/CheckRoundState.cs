﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JasonWeimann.StateMachine;

namespace BigTwo
{
    public class CheckRoundState : IState
    {
        private GameplayData _gameplayData;

        private bool _isRoundFinished;

        public bool IsRoundFinished => _isRoundFinished;

        public CheckRoundState(GameplayData data)
        {
            _gameplayData = data;
        }

        public void OnEnter()
        {
            if (_gameplayData.CurrentRound.GetCurrentPlayerCount() <= 1)
            {

                _isRoundFinished = true;
            }
            else
            {
                _isRoundFinished = false;
            }
        }

        public IEnumerator CoroutineOnEnter()
        {
            yield break;
        }

        public void OnExit()
        {
            
        }

        public void Tick()
        {
            
        }
    }

}
