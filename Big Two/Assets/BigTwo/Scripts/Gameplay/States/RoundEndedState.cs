﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JasonWeimann.StateMachine;

namespace BigTwo
{
    public class RoundEndedState : IState
    {
        private GameplayData _gameplayData;

        private bool _isSetupCompleted;

        public bool IsSetupCompleted => _isSetupCompleted;

        public RoundEndedState(GameplayData data)
        {
            _gameplayData = data;
        }

        public IEnumerator CoroutineOnEnter()
        {
            _isSetupCompleted = false;

            GameManager manager = GameManager.Instance;
            
            Player roundWinnerPlayer = _gameplayData.CurrentRound.GetLatestPlayedPlayer();

            //set this round winner as starting player next round
            manager.SetPlayerAsStartIndex(roundWinnerPlayer);

            yield return new WaitForSeconds(0.5f);

            //update player avatars
            manager.SetPlayerToHappy(roundWinnerPlayer);
            manager.SetAllPlayerToSadExcept(roundWinnerPlayer);

            //show round ended UI
            manager.ShowRoundEndPanel();

            //clear playCards
            manager.UpdateCenterCards(null, Vector2.zero);

            //remove pass player indicators
            manager.HideAllPlayersPassIndicator();

            yield return new WaitForSeconds(1.5f);

            //hide round ended UI
            manager.HideRoundEndPanel();

            yield return new WaitForSeconds(0.5f);

            //return avatar to normal state
            manager.SetAllPlayerToNormalAvatar();

            _isSetupCompleted = true;
        }

        public void OnEnter()
        {
            
        }

        public void OnExit()
        {
            
        }

        public void Tick()
        {
            
        }
    }
}


