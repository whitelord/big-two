﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JasonWeimann.StateMachine;

namespace BigTwo
{
    public class DivideCardsState : IState
    {
        private int _playerCount;
        private GameplayData _gameplayData;


        public DivideCardsState(int playerCount,GameplayData data)
        {
            _playerCount = playerCount;
            _gameplayData = data;
        }

        public void OnEnter()
        {
            ShuffledPlayingCards playingCards = new ShuffledPlayingCards(_playerCount);
            _gameplayData.DividedPacks = playingCards.CardPacks;
        }

        public IEnumerator CoroutineOnEnter()
        {
            yield break;
        }

        public void OnExit()
        {
            
        }

        public void Tick()
        {
            
        }
    }
}

