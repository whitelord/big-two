﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JasonWeimann.StateMachine;

namespace BigTwo
{
    public class UpdateRoundParticipantState : IState
    {
        private GameplayData _gameplayData;

        public bool Completed { get; set; }

        public UpdateRoundParticipantState(GameplayData data)
        {
            _gameplayData = data;
        }

        public void OnEnter()
        {
            Completed = false;
            _gameplayData.CurrentRound.RemoveCurrentPlayer();
            Completed = true;
        }

        public IEnumerator CoroutineOnEnter()
        {
            yield break;
        }

        public void OnExit()
        {
            
        }

        public void Tick()
        {
            
        }
    }
}


