﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JasonWeimann.StateMachine;

namespace BigTwo
{
    public class DistributeCardsState : IState
    {
        private List<Player> _playerList;
        private GameplayData _gameplayData;

        public bool Completed { get; set; }

        public DistributeCardsState(List<Player> playerList,GameplayData data)
        {
            _playerList = playerList;
            _gameplayData = data;
        }

        public void OnEnter()
        {
            Completed = false;

            List<CardDataPack> dividedPacks = _gameplayData.DividedPacks;
            for (int i = 0; i < _playerList.Count; i++)
            {
                _playerList[i].SetCardDataPack(dividedPacks[i]);
            }

            Completed = true;
        }

        public IEnumerator CoroutineOnEnter()
        {
            yield break;
        }

        public void OnExit()
        {
            
        }

        public void Tick()
        {
            
        }
    }
}


