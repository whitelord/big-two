﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JasonWeimann.StateMachine;

namespace BigTwo
{
    public class HaveWinnerState : IState
    {

        private GameplayData _gameplayData;

        public HaveWinnerState(GameplayData data)
        {
            _gameplayData = data;
        }

        public IEnumerator CoroutineOnEnter()
        {
            GameManager manager = GameManager.Instance;

            manager.IsRestartTriggered = false;
            
            Player winPlayer = _gameplayData.CurrentRound.GetCurrentPlayer();

            //change player avatar
            manager.SetPlayerToHappy(winPlayer);
            manager.SetAllPlayerToSadExcept(winPlayer);

            yield return new WaitForSeconds(1.5f);

            //reset game data
            manager.ResetGameplayData();
            manager.ResetPlayers();

            if (winPlayer.GetType() == typeof(RealPlayer))
            {
                manager.ShowWinPanel();
            }
            else
            {
                manager.ShowLosePanel();
            }
        }

        public void OnEnter()
        {

        }

        public void OnExit()
        {
            
        }

        public void Tick()
        {
            
        }
    }
}


