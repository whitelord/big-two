﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JasonWeimann.StateMachine;

namespace BigTwo
{
    public class CheckWinState : IState
    {
        private GameplayData _gameplayData;

        private bool _hasWinner;
        public bool HasWinner => _hasWinner;

        private bool _completedCheck;
        public bool CompletedCheck => _completedCheck;

        public CheckWinState(GameplayData data)
        {
            _gameplayData = data;
        }

        public void OnEnter()
        {
            _completedCheck = false;
            _hasWinner = false;

            Player currentPlayer = _gameplayData.CurrentRound.GetCurrentPlayer();
            if(currentPlayer.RemainingCardCount <= 0)
            {
                _hasWinner = true;
            }
            _completedCheck = true;
        }

        public IEnumerator CoroutineOnEnter()
        {
            yield break;
        }

        public void OnExit()
        {
            
        }

        public void Tick()
        {
            
        }

        
    }

}

