﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JasonWeimann.StateMachine;

namespace BigTwo
{
    public class PlayerTurnState : IState
    {
        private Player _currentTurnPlayer = null;

        private GameplayData _gameplayData;

        private bool _isTurnCompleted;
        public bool IsTurnCompleted => _isTurnCompleted;

        private bool _isPlayerPass;
        public bool IsPlayerPass => _isPlayerPass;

        public PlayerTurnState(GameplayData data)
        {
            _gameplayData = data;
        }

        public void OnEnter()
        {
            
        }

        public IEnumerator CoroutineOnEnter()
        {
            _isTurnCompleted = false;
            _isPlayerPass = false;

            GameManager manager = GameManager.Instance;

            _currentTurnPlayer = _gameplayData.CurrentRound.GetNextPlayer();

            //set this player to start turn
            _currentTurnPlayer.StartTurn(_gameplayData.CurrentRound.CurrentCardPlay, IsFirstRound());

            //wait till player finish their move
            while (_currentTurnPlayer.IsPlaying) yield return null;

            //player pass the current turn
            if (_currentTurnPlayer.SubmittedPlay == null)
            {
                _isPlayerPass = true;
            }
            //player play the current turn
            else
            {
                //Update the round data
                _gameplayData.CurrentRound.UpdateCurrentPlay(_currentTurnPlayer.SubmittedPlay, _currentTurnPlayer);

                //update cards in game interface
                manager.UpdateCenterCards(_currentTurnPlayer.SubmittedPlay.Cards, _currentTurnPlayer.PlayCardsSpawnPosition);
            }

            _isTurnCompleted = true;
        }

        private bool IsFirstRound()
        {
            return _gameplayData.CurrentRound.RoundNumber == 1;
        }

        public void OnExit()
        {
            _currentTurnPlayer = null;
            _isPlayerPass = false;
        }

        public void Tick()
        {
            
        }
    }

}

