﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JasonWeimann.StateMachine;

namespace BigTwo
{
    public class DetermineFirstRoundTurnState : IState
    {
        private List<Player> _playerList;
        private GameplayData _gameplayData;

        public DetermineFirstRoundTurnState(List<Player> playerList,GameplayData data)
        {
            _playerList = playerList;
            _gameplayData = data;
        }

        public void OnEnter()
        {
            int index = 0;

            while (index < _playerList.Count)
            {
                Player p = _playerList[index];
                if (p.HasLowestCard()) break;

                index++;
            }

            _gameplayData.StartPlayerIndex = index;
        }

        public IEnumerator CoroutineOnEnter()
        {
            yield break;
        }

        public void OnExit()
        {
            
        }

        public void Tick()
        {
            
        }
    }
}

