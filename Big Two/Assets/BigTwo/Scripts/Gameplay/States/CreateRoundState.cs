﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JasonWeimann.StateMachine;

namespace BigTwo
{
    public class CreateRoundState : IState
    {
        private List<Player> _playerList;
        private GameplayData _gameplayData;

        public CreateRoundState(List<Player> playerList,GameplayData data)
        {
            _playerList = playerList;
            _gameplayData = data;
        }

        public void OnEnter()
        {

        }

        public IEnumerator CoroutineOnEnter()
        {
            GameManager manager = GameManager.Instance;

            int currentRound = 1;
            if (_gameplayData.CurrentRound != null)
            {
                currentRound = _gameplayData.CurrentRound.RoundNumber + 1;
            }

            _gameplayData.CurrentRound = null;

            manager.ShowRoundStartPanel(currentRound);

            yield return new WaitForSeconds(1f);

            manager.HideRoundStartPanel();

            yield return new WaitForSeconds(0.5f);

            RoundData data = new RoundData(_playerList, _gameplayData.StartPlayerIndex, currentRound);
            _gameplayData.CurrentRound = data;

            yield break;
        }

        public void OnExit()
        {
            
        }

        public void Tick()
        {
            
        }
    }
}

