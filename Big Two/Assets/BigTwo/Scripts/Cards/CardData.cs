﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

namespace BigTwo
{

    [Serializable]
    public class CardData
    {
        public int Value;

        public CardType CardType;

        public Sprite SpriteImage;

    }

}


