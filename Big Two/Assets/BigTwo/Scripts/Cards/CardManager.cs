﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwo
{
    public class CardManager : MonoBehaviour
    {
        private static CardManager _instance;

        public static CardManager Instance => _instance;

        [SerializeField] private Sprite _closedCardSprite;
        [SerializeField] private List<CardData> _cardList;

        private CardData _lowestCardValue;

        public Sprite CloseCardSprite => _closedCardSprite;

        private void Awake()
        {
            _instance = this;
        }

        private void Start()
        {
            
        }

        public int GetMaximumCardCount() => _cardList.Count;

        public CardData GetCardDataAtIndex(int index)
        {
            if (index < 0 || index > _cardList.Count - 1) return null;

            return _cardList[index];
        }

        public CardData GetLowestCardValue()
        {
            if(_lowestCardValue == null)
            {
                _lowestCardValue = new CardData()
                {
                    Value = 3,
                    CardType = CardType.Diamond
                };
            }
            
            return _lowestCardValue;
        }

    }
}


