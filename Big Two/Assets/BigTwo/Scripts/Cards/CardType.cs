﻿namespace BigTwo
{
    public enum CardType
    {
        Diamond = 1,
        Clubs = 2,
        Hearts = 3,
        Spades = 4
    }

}
