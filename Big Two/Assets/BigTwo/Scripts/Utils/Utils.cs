﻿using System.Collections.Generic;
using System.Linq;

namespace BigTwo
{
    public class Utils
    {

        public static List<CardData> SortCardDataList(List<CardData> data)
        {
            data = data.OrderBy(x => x.Value).ThenBy(x => x.CardType).ToList();
            return data;
        }

    }
}


