﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwo{
    public class MainMenu : MonoBehaviour
    {
        [SerializeField] private GameObject avatarSelectionPanel;
        [SerializeField] private GameObject startButton;

        private void Awake()
        {
            Application.targetFrameRate = 60;
        }

        public void StartGameButtonClicked()
        {
            AudioController.Play(AudioKeys.SFX_CLICK);
            startButton.SetActive(false);
            avatarSelectionPanel.SetActive(true);
        }
    }
}


