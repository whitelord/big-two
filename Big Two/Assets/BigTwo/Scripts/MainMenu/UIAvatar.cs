﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BigTwo
{
    [RequireComponent(typeof(Toggle))]
    public class UIAvatar : MonoBehaviour
    {
        public delegate void SelectedHandler(AvatarData data);
        public static event SelectedHandler OnSelected;

        [SerializeField] private Image _mainImage;

        private Toggle _toggle;
        private AvatarData _data;

        private void Awake()
        {
            _toggle = GetComponent<Toggle>();
        }

        public void Init(AvatarData data,ToggleGroup group)
        {
            _data = data;

            _toggle.group = group;

            SetUI();

            gameObject.SetActive(true);
        }

        private void SetUI()
        {
            if (_data == null) return;
            _mainImage.sprite = _data.MainAvatar;
        }

        public void ToggleSelected(bool val)
        {
            if (val)
            {
                OnSelected?.Invoke(_data);
            }
            
        }
    }
}


