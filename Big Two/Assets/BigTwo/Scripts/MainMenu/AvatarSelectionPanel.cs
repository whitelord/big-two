﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace BigTwo
{
    public class AvatarSelectionPanel : MonoBehaviour
    {

        [SerializeField] private GameObject _UIAvatarPrefab;
        [SerializeField] private GameObject _parentGroup;
        [SerializeField] private ToggleGroup _toggleGroup;
        [SerializeField] private GameObject _errorNoSeletedAvatarObject;

        private AvatarData _selectedAvatarData;

        private void Start()
        {
            CreateUIAvatars();
        }

        private void OnEnable()
        {
            UIAvatar.OnSelected += OnUIAvatarSelected;
        }

        private void OnDisable()
        {
            UIAvatar.OnSelected -= OnUIAvatarSelected;
        }

        private void CreateUIAvatars()
        {
            List<AvatarData> avatarList = AvatarManager.Instance.AvatarDataList;

            if (avatarList == null) return;

            foreach (AvatarData data in avatarList)
            {
                CreateUIAvatar(data);
            }
        }

        private void CreateUIAvatar(AvatarData data)
        {
            //instantiate object
            GameObject g = Instantiate(_UIAvatarPrefab);
            g.transform.SetParent(_parentGroup.transform);
            g.transform.localScale = Vector3.one;


            //get UIAvatar and Initialize
            UIAvatar uiAvatar = g.GetComponent<UIAvatar>();
            uiAvatar.Init(data, _toggleGroup);
        }

        private void OnUIAvatarSelected(AvatarData data)
        {
            AudioController.Play(AudioKeys.SFX_CLICK);
            _selectedAvatarData = data;
        }

        public void StartGameButtonClicked()
        {
            AudioController.Play(AudioKeys.SFX_CLICK);

            if (!IsAnyAvatarSelected())
            {
                ShowErrorNoSelectedAvatar();
                return;
            }

            gameObject.SetActive(false);

            PlayerData.Instance.SelectedAvatar = _selectedAvatarData;

            SceneManager.LoadScene(SceneConstants.SCENE_GAME);

        }

        private bool IsAnyAvatarSelected()
        {
            if (_selectedAvatarData == null || _selectedAvatarData.AvatarID == "")
            {
                return false;
            }

            return true;
        }

        private void ShowErrorNoSelectedAvatar() => _errorNoSeletedAvatarObject.SetActive(true);

        private void HideErrorNoSeletedAvatar() => _errorNoSeletedAvatarObject.SetActive(false);
    }

}

