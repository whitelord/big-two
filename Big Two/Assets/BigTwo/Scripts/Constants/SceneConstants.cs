﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwo
{
    public class SceneConstants
    {
        public const int SCENE_MENU = 0;
        public const int SCENE_GAME = 1;
    }
}


