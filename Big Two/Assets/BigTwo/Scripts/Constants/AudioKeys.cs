﻿
namespace BigTwo
{
    public class AudioKeys
    {
        public const string SFX_CLICK = "click";
        public const string MUSIC_MAIN = "music";
        public const string SFX_WIN = "won";
        public const string SFX_LOSE = "lose";
        public const string SFX_CARD_DRAW = "cardSlide";
    }
}


