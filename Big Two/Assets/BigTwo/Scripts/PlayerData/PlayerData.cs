﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwo
{
    /// <summary>
    /// All player related data will be store here.
    /// </summary>
    public class PlayerData
    {
        private static PlayerData _instance = new PlayerData();

        public static PlayerData Instance => _instance;

        private AvatarData _selectedAvatar;

        public AvatarData SelectedAvatar
        {
            get => _selectedAvatar;
            set => _selectedAvatar = value;
        }

    }
}


