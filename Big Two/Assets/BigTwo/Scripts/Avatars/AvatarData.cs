﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace BigTwo
{
    [Serializable]
    public class AvatarData 
    {
        public string AvatarID;
        public Sprite MainAvatar;
        public Sprite SadAvatar;
        public Sprite HappyAvatar;

        //TODO: Change sprite reference to unity addressable system
    }
}


