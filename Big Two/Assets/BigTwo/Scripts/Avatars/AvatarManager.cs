﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BigTwo
{
    public class AvatarManager : MonoBehaviour
    {
        private static AvatarManager _instance;

        public static AvatarManager Instance => _instance;

        [SerializeField] private List<AvatarData> _avatarDataList;

        public List<AvatarData> AvatarDataList => _avatarDataList;

        private void Awake()
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }

        public AvatarData GetRandomAvatar()
        {
            if (_avatarDataList == null) return null;

            return _avatarDataList[Random.Range(0, _avatarDataList.Count)];
        }

    }

}


