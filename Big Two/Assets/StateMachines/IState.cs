﻿using UnityEngine;
using System.Collections;

namespace JasonWeimann.StateMachine
{
    public interface IState
    {
        /// <summary>
        /// Called every frame like MonoBehaviour Update()
        /// </summary>
        void Tick();

        /// <summary>
        /// Called once When entering state
        /// </summary>
        void OnEnter();

        /// <summary>
        /// Called once when entering state
        /// Use this if you want to delay some work instead of OnEnter()
        /// </summary>
        /// <returns></returns>
        IEnumerator CoroutineOnEnter();


        /// <summary>
        /// Called once when exiting the state
        /// </summary>
        void OnExit();
    }
}

